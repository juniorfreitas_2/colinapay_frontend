import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

//import sweetalert2
// import swal from 'sweetalert2'
// window.Swal = swal;
// //import axios
// window.axios = require('axios');

//import jquery
global.jQuery = require('jquery');
window.$ = global.jQuery;


// not loading
// import('./assets/theme/js/throttle/throttle')
// import('bootbox')
import('./assets/theme/js/app.bundle')
import('./assets/theme/js/vendors.bundle')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
