import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Dashboard/Home.vue'

import productRoutes from '../views/Products/Routes'
import IndexProducts from '../views/Products/IndexProduct'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/Profile/IndexProfile.vue')
  },
  {
    path: '/products',
    name: 'Produtos',
    component: IndexProducts,
    children: productRoutes
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
