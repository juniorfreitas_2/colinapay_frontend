import CreateProduct from '../../components/Produto/CreateProduct'
import ListProduct from '../../components/Produto/ListProduct'
import ViewProduct from '../../components/Produto/ViewProduct'

const productRoutes = [
    {
        path: '/',
        name: 'ListProduct',
        component: ListProduct
    },
    {
        path: 'create',
        name: 'CreateProduct',
        component: CreateProduct
    },
    {
        path: 'view/:id',
        name: 'ViewProduct',
        component: ViewProduct
    }
]

export default productRoutes